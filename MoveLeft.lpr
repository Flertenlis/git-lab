procedure MoveLeft(var Arr: Mass; Min: Integer);
   var
       Index: Integer;
   begin
      for Index := 1 to NumberOfElements do
    begin
      if (Arr[Index] = Min) then
        begin
          AlternateIndex := Index;
            while (AlternateIndex > 1) and (Arr[AlternateIndex - 1] <> Min) do
                begin
                    Arr[AlternateIndex] := Arr[AlternateIndex - 1];
                    Dec(AlternateIndex);
                end;
            Arr[AlternateIndex] := Min;
          end;
      end;
   end;


