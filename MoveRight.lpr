procedure MoveRight(var Arr: Mass);
   var
       Index: Integer;
   begin
     for Index := 1 to NumberOfElements do
              begin
                  if (Arr[Index] = 0) then
                    begin
                       for AlternateIndex := Index + 1 to NumberOfElements do
                           begin
                               Temp := Arr[AlternateIndex];
                               Arr[AlternateIndex] := Arr[AlternateIndex - 1];
                               Arr[AlternateIndex - 1] := Temp
                           end;
                    end;
              end;
   end;

