program prrr;
const
  NumberOfElements = 18;
type
  Mass = array[1..NumberOfElements] of integer;

var
  Index, AlternateIndex, Multiplication, Temp, Min: Integer;
  Arr: Mass;

begin
  Randomize;
  CreateArray(Arr, NumberOfElements);
  WriteLn('Array 1');
  PrintArray(Arr, NumberOfElements);
  WriteLn;
  WriteLn;
  Multiplication := 1;

  for Index := 1 to NumberOfElements do
      begin
        Multiplication := Multiplication * Arr[Index];
      end;

  if (Multiplication <> 0) then
      begin
          WriteLn('Array has no "0"');
          WriteLn;
      end
  else
      begin
          MoveRight(Arr);
          WriteLn('Array 2');
          PrintArray(Arr, NumberOfElements);
          WriteLn;
          WriteLn;
      end;

  Min := Arr[1];
  SearchMinimal(Arr, Min);
  MoveLeft(Arr, Min);
  WriteLn('Array 3');
  PrintArray(Arr, NumberOfElements);
  ReadLn;

end.

